package code.data

import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("text") var text: String? = null,
)
