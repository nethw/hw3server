package code.api

import code.data.Product
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.io.BufferedReader
import java.io.File
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.servlet.http.HttpServletRequest

class RString : Runnable {
    @Volatile
    var inputString: String? = null

    override fun run() {
    }
}

class Worker(val filename: String) : Runnable {
    private var inputString: RString = RString()

    fun getString() : String? {
        return inputString.inputString
    }

    override fun run() {
        println("Start operation on: $filename")
        try {
            val bufferedReader: BufferedReader = File("files/$filename").bufferedReader()
            inputString.inputString = bufferedReader.use { it.readText() }
            println("Found file: $filename")
        } catch (e: Exception) {
            println("No file: $filename")
        }
    }
}

@RestController
class Api {
    @GetMapping("/code/get-file")
    fun get(@RequestParam("filename") filename: String, request: HttpServletRequest): Product {
        val worker = Worker(filename)

        executor.execute(worker)
        try {
            executor.awaitTermination(2, TimeUnit.SECONDS);
        } catch (e : Exception) {
            throw ResponseStatusException(
                HttpStatus.NOT_FOUND, "file not found"
            )
        }
        val inputString = worker.getString()
        inputString
            ?: throw ResponseStatusException(
                HttpStatus.NOT_FOUND, "file not found"
            )
        return Product(inputString)
    }

    companion object {
        private const val maxConnections = 2

        val executor = Executors.newFixedThreadPool(maxConnections)
    }
}
